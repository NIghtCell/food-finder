import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-recipe-input',
  templateUrl: './recipe-input.component.html',
  styleUrls: ['./recipe-input.component.scss'],
})
export class RecipeInputComponent implements OnInit {
  ingredients = new FormControl('');
  @Output() enter: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onSearchInputEnter(): void {
    const ingredientsList = this.ingredients.value;
    this.enter.emit(ingredientsList);
  }
}
