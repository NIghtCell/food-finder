import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from '../shared/model/recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss'],
})
export class RecipeListComponent implements OnInit {
  @Input() recipes: Recipe[] | undefined;

  constructor() {}

  ngOnInit(): void {}
}
