import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Recipe } from '../model/recipe.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RecipeService {
  private static API_KEY = '849579104fae4bd788cebb12b4462b4f';

  constructor(private http: HttpClient) {}

  private static mapRecipes(recipes: Recipe[]): Recipe[] {
    return recipes.map((recipe) => ({
      ...recipe,
      ingredients: [...recipe.missedIngredients, ...recipe.usedIngredients],
    }));
  }

  public findRecipeByIngredients(ingredients: string): Observable<Recipe[]> {
    let params = new HttpParams();
    params = params.append('apiKey', RecipeService.API_KEY);
    params = params.append('ingredients', ingredients);
    return this.http
      .get<Recipe[]>('https://api.spoonacular.com/recipes/findByIngredients', {
        params: params,
      })
      .pipe(map((recipes) => RecipeService.mapRecipes(recipes)));
  }
}
