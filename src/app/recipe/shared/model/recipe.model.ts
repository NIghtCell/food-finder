export interface Recipe {
  id: number;
  image: string;
  title: string;
  missedIngredients: any;
  usedIngredients: any;
  ingredients: any;
}
