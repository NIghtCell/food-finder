import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RecipeInputComponent } from 'src/app/recipe/recipe-input/recipe-input.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { RecipeService } from './shared/service/recipe.service';
import { RecipeComponent } from './recipe.component';
import { RecipeCardComponent } from './recipe-card/recipe-card.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [
    RecipeComponent,
    RecipeInputComponent,
    RecipeListComponent,
    RecipeCardComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [RecipeService],
  exports: [RecipeComponent],
})
export class RecipeModule {}
