import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RecipeService } from './shared/service/recipe.service';
import { Recipe } from './shared/model/recipe.model';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss'],
})
export class RecipeComponent implements OnInit {
  recipes$: Observable<Recipe[]> | undefined;
  constructor(private recipeService: RecipeService) {}

  onRecipeInputEnter($ingredientsList: string): void {
    this.getRecipesByIngredients($ingredientsList);
  }

  private getRecipesByIngredients(ingredients: string): void {
    this.recipes$ = this.recipeService.findRecipeByIngredients(ingredients);
  }

  ngOnInit(): void {}
}
